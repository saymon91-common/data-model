import { validate } from '..';
import { DataModel } from '../lib/types';

const model: DataModel = [
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'position.coordinates.0',
    nullable: false,
    storeKey: 'latitude',
  },
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'position.coordinates.1',
    nullable: false,
    storeKey: 'latitude',
  },
  {
    dataType: 'number',
    defaultValue: 0,
    fieldType: 'state',
    key: 'position.coordinates.1',
    nullable: false,
    storeKey: 'altitude',
  },
  {
    dataType: 'string',
    defaultValue: 'Point',
    fieldType: 'state',
    key: 'position.type',
    nullable: false,
  },
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'speed',
    nullable: false,
    storeKey: 'speed',
  },
];

// console.log(validate(model));

test('VALIDATION', () =>
  expect(validate(model)).toEqual([
    new Error('Repeatable Node with key: position.coordinates.1'),
    new Error('Repeatable Node with storeKey: latitude'),
  ]));
