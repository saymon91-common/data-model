import { toStore } from '..';
import { DataModel, IObject } from '../lib/types';

const model: DataModel = [
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'position.coordinates.0',
    nullable: false,
    storeKey: 'latitude',
  },
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'position.coordinates.1',
    nullable: false,
    storeKey: 'longitude',
  },
  {
    dataType: 'number',
    defaultValue: 0,
    fieldType: 'state',
    key: 'position.coordinates.2',
    nullable: false,
    storeKey: 'altitude',
  },
  {
    dataType: 'string',
    defaultValue: 'Point',
    fieldType: 'state',
    key: 'position.type',
    nullable: false,
  },
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'speed',
    nullable: false,
    storeKey: 'speed',
  },
];

const storeData: IObject = {
  state: {
    position: {
      coordinates: [50.32345354, 32.34647575, 0],
      type: 'Point',
    },
    speed: 120,
  },
};

test('TO STORE', () =>
  expect(toStore(model, storeData)).toEqual({
    altitude: 0,
    latitude: 50.32345354,
    longitude: 32.34647575,
    speed: 120,
  }));
