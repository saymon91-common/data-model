import { pick } from '..';
import { DataModel } from '../lib/types';

const model: DataModel = [
  {
    dataType: 'string',
    fieldType: 'config',
    key: 'test',
    nullable: false,
  },
  {
    dataType: 'number',
    fieldType: 'state',
    key: 'test1',
  },
];

test('PICK', () => {
  expect(pick({ fieldType: 'state' }, model)).toEqual([
    {
      dataType: 'number',
      fieldType: 'state',
      key: 'test1',
    },
  ]);
  expect(pick(model, { fieldType: 'config' })).toEqual([
    {
      dataType: 'string',
      fieldType: 'config',
      key: 'test',
      nullable: false,
    },
  ]);
  expect(pick(model, { nullable: false })).toEqual([
    {
      dataType: 'string',
      fieldType: 'config',
      key: 'test',
      nullable: false,
    },
  ]);
});
