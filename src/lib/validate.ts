import { DataModel, INode } from './types';

interface IReduceResult {
  repeatable: INode[];
  unique: DataModel;
}

const validateUnique = (field: keyof INode, model: DataModel, result: string[] = []): Error[] => {
  if (!model.length) {
    return result.map(Error);
  }

  const [current, ...tail] = model;

  const { repeatable, unique } = tail.reduce<IReduceResult>(
    (res, node) => {
      res[node[field] === current[field] ? 'repeatable' : 'unique'].push(node);
      return res;
    },
    { repeatable: [], unique: [] },
  );

  return validateUnique(field, unique, [
    ...result,
    ...new Set(repeatable.map(({ [field]: f }) => `Repeatable Node with ${field}: ${f}`)),
  ]);
};

export const validate = (model: DataModel): Error[] => {
  return [...validateUnique('key', model), ...validateUnique('storeKey', model)];
};
