import { converter } from '@prof-itgroup/data-converter/lib/converter';
import { IDataConvertModel } from '@prof-itgroup/data-converter/lib/types';

import { DataModel, IFlatObject, INode, IObject } from './types';

const modelReducer = (
  res: IDataConvertModel,
  { storeKey: from, key, defaultValue, dataType: type, fieldType }: INode,
): IDataConvertModel => [...res, { from, to: `${fieldType}.${key}`, defaultValue, type }];

export const fromStore = (model: DataModel, data: IFlatObject): IObject =>
  converter(model.reduce<IDataConvertModel>(modelReducer, []), data);
