import { converter } from '@prof-itgroup/data-converter/lib/converter';
import { IDataConvertModel } from '@prof-itgroup/data-converter/lib/types';

import { DataModel, IFlatObject, INode, IObject } from './types';

const modelReducer = (
  res: IDataConvertModel,
  { storeKey: to, key, defaultValue, dataType: type, fieldType }: INode,
): IDataConvertModel => (to ? [...res, { from: `${fieldType}.${key}`, to, defaultValue, type }] : res);

export const toStore = (model: DataModel, data: IObject): IFlatObject =>
  converter(model.reduce<IDataConvertModel>(modelReducer, []), data);
