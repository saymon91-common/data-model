import { isMatch } from 'lodash';

import { DataModel, INode } from './types';

export const pick = <T extends Partial<INode>>(condition: T | DataModel, model: DataModel | T): DataModel => {
  const [modelInstance, condInstance]: [DataModel, T] = Array.isArray(model)
    ? [model as DataModel, condition as T]
    : [condition as DataModel, model as T];
  return modelInstance.filter(node => isMatch(node, condInstance));
};
