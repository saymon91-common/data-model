export type DataTypeNames = 'string' | 'number' | 'boolean';
export type DataTypes = string | number | boolean | null;
export type NullableDataTypes = DataTypes | null;
export type FieldType = 'const' | 'config' | 'virtual' | 'state';

export interface INode<
  D extends NullableDataTypes = NullableDataTypes,
  N extends boolean = boolean,
  S extends string | never = string
> {
  key: string;
  dataType: DataTypeNames;
  fieldType: FieldType;
  nullable?: N;
  storeKey?: S;
  defaultValue?: D;
}

export interface IConstantNode<T extends DataTypes = DataTypes> extends INode<DataTypes, false, never> {
  fieldType: 'const';
  defaultValue: DataTypes;
}

export interface IConfigNode extends INode {
  fieldType: 'config';
}

export interface IVirtualNode extends INode {
  fieldType: 'virtual';
}

export interface IStateNode extends INode {
  fieldType: 'state';
}

export type DataModel = Array<IConstantNode | IVirtualNode | IStateNode | IConfigNode>;

export interface IFlatObject {
  [K: string]: DataTypes;
}

export type IObject = {
  [K in FieldType]?: any;
};
